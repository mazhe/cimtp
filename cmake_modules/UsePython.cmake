macro(ADD_PYTHON_SCRIPT target source)
  add_custom_command(
      OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/${target}
      COMMAND ${CMAKE_COMMAND} -DINPUT=${CMAKE_CURRENT_SOURCE_DIR}/${source} -DOUTPUT=${CMAKE_CURRENT_BINARY_DIR}/${target} -P ${CMAKE_BINARY_DIR}/configure_python_script.cmake
      COMMAND chmod 755 ${CMAKE_CURRENT_BINARY_DIR}/${target}
      DEPENDS ${source}
      COMMENT "Configuring Python script ${target}"
      VERBATIM)
  add_custom_target(configure_${target} ALL DEPENDS ${CMAKE_CURRENT_BINARY_DIR}/${target})
endmacro()
