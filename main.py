import base64, gzip, io, mimetypes, netrc, os, platform, posixpath, re, string, signal, ssl, sys, tempfile, xml.etree.ElementTree
if sys.version_info[0] >= 3:
    import http.client as httpclient
else:
    import httplib as httpclient

timeout = 60
retry = 3

# Uber-simple webdav class 
class webdav(httpclient.HTTPSConnection):
    # Update internal infos
    def set_auth(self, auth):
        self.headers = {
            "Accept-Encoding": "gzip",
            "User-Agent": "cimtp/0 (%s %s)" % (os.uname()[0], os.uname()[2]),
            "Connection": "keep-alive",
        }
        if sys.version_info[0] >= 3:
            self.headers["Authorization"] = b"Basic "+base64.b64encode(bytes(auth[0]+":"+auth[2], "utf-8"))
        else:
            self.headers["Authorization"] = "Basic "+base64.b64encode(auth[0]+":"+auth[2])

    # Function that mimics python's os.path functions
    def exists(self, path):
        headers2 = self.headers.copy()
        headers2["Depth"] = "0"
        self.request("PROPFIND", path, None, headers2)
        r = self.getresponse()
        r.read()
        if r.status == 404:
            return False
        elif r.status != 207:
            raise Exception(r.reason)
        else:
            return True
    def isdir(self, path):
        headers2 = self.headers.copy()
        headers2["Depth"] = "0"
        self.request("PROPFIND", re.sub("^/cigri", "/irods-webdav", path), None, headers2)
        r = self.getresponse()
        if r.status == 404:
            r.read()
            return False
        if r.status != 207:
            raise Exception(r.reason)
        if r.getheader("Content-Encoding") == "gzip":
            gzf = gzip.GzipFile(fileobj=io.BytesIO(r.read()))
            root = xml.etree.ElementTree.fromstring(gzf.read())
        else:
            root = xml.etree.ElementTree.fromstring(r.read())
        return root.find("{DAV:}response/{DAV:}propstat/{DAV:}prop/{DAV:}resourcetype/{DAV:}collection") != None
    def _walk(self, path):
        dirs, nondirs = [], []
        for e in self.ls(path):
            if self.isdir(e):
                dirs.append(posixpath.basename(re.sub("/$", "", e)))
            else:
                nondirs.append(posixpath.basename(e))
        yield path, dirs, nondirs
        for d in dirs:
            path2 = posixpath.join(path, d)
            #yield from self.walk(path2)
            for t in self.walk(path2):
                yield t
    def walk(self, path):
        w = None
        for i in range(retry-1):
            try:
                w = self._walk(path)
            except httpclient.RemoteDisconnected:
                self.connect()
                continue
            except:
                continue
            else:
                return w
        return self._walk(path)

    # Functions that mimics SFTP commands
    def mkdir(self, path):
        self.request("MKCOL", path, None, self.headers)
        r = self.getresponse()
        r.read()
        if r.status != 201:
            raise Exception(r.reason)
    def _get(self, rpath, lpath):
        self.request("GET", rpath, None, self.headers)
        r = self.getresponse()
        if r.status != 200:
            raise Exception(r.reason)
        if r.getheader("Content-Encoding") == "gzip":
            gzf = gzip.GzipFile(fileobj=io.BytesIO(r.read()))
            open(lpath, "wb").write(gzf.read())
        else:
            open(lpath, "wb").write(r.read())
    def get(self, rpath, lpath):
        for i in range(retry-1):
            try:
                self._get(rpath, lpath)
            except httpclient.RemoteDisconnected:
                self.connect()
                continue
            except:
                continue
            else:
                return
        self._get(rpath, lpath)
    def ls(self, path):
        headers2 = self.headers.copy()
        headers2["Depth"] = "1"
        self.request("PROPFIND", path, None, headers2)
        r = self.getresponse()
        if r.status != 207:
            raise Exception(r.reason)
        if r.getheader("Content-Encoding") == "gzip":
            gzf = gzip.GzipFile(fileobj=io.BytesIO(r.read()))
            root = xml.etree.ElementTree.fromstring(gzf.read())
        else:
            root = xml.etree.ElementTree.fromstring(r.read())
        if sys.version_info[0] >= 3 \
                or (sys.version_info[0] == 2 and sys.version_info[1] >= 7):
            entries = root.findall("{DAV:}response/{DAV:}href", {"DAV": ""})
        else:
            entries = root.findall("{DAV:}response/{DAV:}href")
        res = []
        for e in entries[1::]:
            res.append(e.text)
        return res
    def _put(self, lpath, rpath):
        headers2 = self.headers.copy()
        headers2["Content-Type"] = mimetypes.guess_type(lpath)[0] or "application/octet-stream"
        self.request("PUT", rpath, open(lpath, "rb"), headers2)
        r = self.getresponse()
        r.read()
        if not (r.status == 200 or r.status == 201 or r.status == 204):
            raise Exception(r.reason)
    def put(self, lpath, rpath):
        for i in range(retry-1):
            try:
                self._put(lpath, rpath)
            except httpclient.RemoteDisconnected:
                self.connect()
                continue
            except:
                continue
            else:
                return
        self._put(lpath, rpath)
    def rm(self, path):
        if self.isdir(path):
            raise Exception(path+" is a directory")
        headers2 = self.headers.copy()
        headers2["Depth"] = "0"
        self.request("DELETE", path, None, headers2)
        r = self.getresponse()
        r.read()
        if r.status != 204:
            raise Exception(r.reason)
    def rmr(self, path):
        headers2 = self.headers.copy()
        headers2["Depth"] = "infinity"
        self.request("DELETE", path, None, headers2)
        r = self.getresponse()
        r.read()
        if r.status != 204:
            raise Exception(r.reason)

def sighandler(signum, frame):
    exit(1)

def main():
    signal.signal(signal.SIGINT, sighandler)

    args = docopt(__doc__)

    auth = netrc.netrc(os.path.expanduser("~")+"/.netrc").authenticators("ciment-grid.ujf-grenoble.fr")
    if not auth or not auth[0] or not auth[2]:
        raise Exception("Cannot retrieve credentials from netrc file")

    if hasattr(ssl, "create_default_context"):
        sslctx = ssl.create_default_context(cadata="""\
-----BEGIN CERTIFICATE-----
MIIBwzCCASwCCQDeEIGDGzJIxDANBgkqhkiG9w0BAQUFADAmMSQwIgYDVQQDExtj
aW1lbnQtZ3JpZC51amYtZ3Jlbm9ibGUuZnIwHhcNMTAxMjEzMTYyMDAwWhcNMjAx
MjEwMTYyMDAwWjAmMSQwIgYDVQQDExtjaW1lbnQtZ3JpZC51amYtZ3Jlbm9ibGUu
ZnIwgZ8wDQYJKoZIhvcNAQEBBQADgY0AMIGJAoGBAMeYjNgYXCBe59tZqgy6x2R4
WjNBnPyyoiJ/qvHHjkuFiy/4IM+EfnsaInegJN9eNhkXPgy83d6Fg5lU8VJXzgLB
FmUqMKFiT0pd6bB8Z7zQDf1WgGfD8h8rpag6+ngSSxhmg0IHm4yfJ0x9TEWJjYMp
Z49mLucfTRTEE2TZUhrxAgMBAAEwDQYJKoZIhvcNAQEFBQADgYEAe1o41PYD/xg3
03iCmFpddgtDtuiKBHQgIcU7V41LARTm7Shd164jrHgJEXGCcIDFo4e7VEn1Mbsd
c3pNkAp0TP2SBFLJXXX528oFgvmahdOstoADd5v8n+Xef27YNl6wuKdAryZNkETV
GS6T8mDjE+LOT2cpGm7jsCbK1iPLykQ=
-----END CERTIFICATE-----
""")
        conn = webdav("ciment-grid.ujf-grenoble.fr", timeout=timeout, context=sslctx)
    else:
        conn = webdav("ciment-grid.ujf-grenoble.fr", timeout=timeout)
    conn.set_debuglevel(0)
    conn.set_auth(auth)

    if args["<lpath>"] == None:
        args["<lpath>"] = os.getcwd()
    if args["<rpath>"] == None:
        args["<rpath>"] = "/irods-webdav/home/"+auth[0]
    elif not args["<rpath>"].startswith("/"):
        args["<rpath>"] = "/irods-webdav/home/"+auth[0]+"/"+args["<rpath>"]

    if args["get"]:
        if os.path.exists(args["<lpath>"]):
            args["<lpath>"] = os.path.join(args["<lpath>"], posixpath.basename(args["<rpath>"]))

        if conn.isdir(args["<rpath>"]):
            if args["-r"] == None:
                raise Exception(args["<rpath>"]+"is a directory")
            if not os.path.exists(args["<lpath>"]):
                os.mkdir(args["<lpath>"])
            for root, dirs, files in conn.walk(args["<rpath>"]):
                root2 = re.sub(args["<rpath>"]+"/?", "", root)
                for d in dirs:
                    if not os.path.exists(os.path.join(args["<lpath>"], root2, d)):
                        os.mkdir(os.path.join(args["<lpath>"], root2, d))
                for f in files:
                    if args["-q"] != True:
                        print(re.sub("^/irods-webdav", "/cigri", args["<rpath>"])+"/"+root2+"/"+f)
                    conn.get(args["<rpath>"]+"/"+root2+"/"+f,
                             os.path.join(args["<lpath>"],
                                          root2,
                                          f))
        else:
            if args["-q"] != True:
                print(re.sub("^/irods-webdav", "/cigri", args["<rpath>"]))
            conn.get(args["<rpath>"], args["<lpath>"])

    elif args["ls"]:
        args["<rpath>"] = re.sub("^/cigri", "/irods-webdav", args["<rpath>"])
        print("\n".join(conn.ls(args["<rpath>"])))

    elif args["put"]:
        if conn.exists(args["<rpath>"]):
            args["<rpath>"] = args["<rpath>"]+"/"+os.path.basename(args["<lpath>"])
        if os.path.isdir(args["<lpath>"]):
            if args["-r"] == None:
                raise Exception(args["<lpath>"]+"is a directory")
            if not conn.exists(args["<rpath>"]):
                conn.mkdir(args["<rpath>"])
            for root, dirs, files in os.walk(args["<lpath>"]):
                root2 = re.sub(args["<lpath>"]+"/?", "", root)
                for d in dirs:
                    if not conn.exists(args["<rpath>"]+"/"+root2+"/"+d):
                        conn.mkdir(args["<rpath>"]+"/"+root2+"/"+d)
                for f in files:
                    if args["-q"] != True:
                        print(os.path.join(args["<lpath>"], root2, f))
                    conn.put(os.path.join(args["<lpath>"], root2, f),
                             args["<rpath>"]+"/"+root2+"/"+f)
        else:
            if args["-q"] != True:
                print(args["<lpath>"])
            conn.put(args["<lpath>"], args["<rpath>"])

    elif args["rm"]:
        if args["-r"] != None:
            conn.rmr(args["<rpath>"])
        else:
            conn.rm(args["<rpath>"])

    conn.close()
    exit(0)

if __name__ == "__main__":
    main()
