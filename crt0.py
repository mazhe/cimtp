#!@PYTHON_EXECUTABLE@
# -*- coding: utf-8 -*-
"""\
cimtp.py
--------
CIMENT Irods Webdav Client, basic commands to interact with CIMENT grid 
grid storage.

Authentication is done through .netrc file, as defined in ftp(1) manpage,
be sure to put a ciment-grid.ujf-grenoble.fr machine entry.

Usage:
  cimtp.py ls [<rpath>]
  cimtp.py get [-r] [-q] <rpath> [<lpath>]
  cimtp.py put [-r] [-q] <lpath> [<rpath>]
  cimtp.py rm [-r] [<rpath>]
  cimtp.py -h | --help

Options:
  -r              Recursively copy entire directories when uploading and 
                  downloading, or remove.
  -q              Quiet mode: disables status messages
  -h --help       Show this screen.

"""


