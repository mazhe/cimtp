 cimtp.py
=========
CIMENT Irods Webdav Client, basic commands to interact with CIMENT grid 
grid storage.

cimtp is CeCILL-B licensed, see COPYRIGHT or COPYRIGHT_FR


 Requirements
=============
cimtp is written in Python (<https://www.python.org/>), version 3 and more
is recommended, but version 2.6+ is supported on a best-effort basis.

You will also need CMake (<http://www.cmake.org>) with a working backend 
(may I suggest make?) to build/install the script.


 How to build/install
=====================
This project use CMake (<http://www.cmake.org>) to build, available on all
supported platforms.

Here are the minimal instructions to build the script, change directory (cd)
to cimtp source tree and do:

  $ mkdir build
  $ cd build
  $ cmake ..
  $ make

There are two options you may want to specify with -D when calling cmake, the
first one is PYTHON_EXECUTABLE to select a certain python interpreter path 
(this path will be hardcoded in the script). Only use absolute path, eg:

  $ cmake -DPYTHON_EXECUTABLE=/usr/local/bin/python3.5 ..

You may also want to set CMAKE_INSTALL_PREFIX to select where the script
will be installed:

  $ cmake -DCMAKE_INSTALL_PREFIX=$HOME/.local ..

You can obviously use both:

  $ cmake -DCMAKE_INSTALL_PREFIX=$HOME/.local -DPYTHON_EXECUTABLE=/usr/local/bin/python3.5 ..

Finally, install the script:

  $ make install


 Configuration
==============
Authentication is done through .netrc file, as defined in ftp(1) manpage,
be sure to put a ciment-grid.ujf-grenoble.fr machine entry.

You may already have one if you used programs like cadaver and such, here's
an example:

machine ciment-grid.ujf-grenoble.fr
  login mycimentlogin
  password mycimentpassword

Do I need to tell that such a file must be set with rw-,---,--- permissions?


 Usage
======
See

  $ cimtp.py --help
